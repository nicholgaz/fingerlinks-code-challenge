//
//  Root.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
struct Root: Codable {
    let status: String
    let totalResults: Int
    let articles: [Article]?
}
