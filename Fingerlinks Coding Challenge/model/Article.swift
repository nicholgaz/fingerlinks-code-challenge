//
//  Article.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

struct Article: Codable {
    let source: Source
    let author : String
    let title: String
    let description : String
    let url: URL
    let urlToImage: URL
    let publishedAt: Date
    let content: String
}
