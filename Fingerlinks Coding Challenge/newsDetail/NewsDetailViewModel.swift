//
//  NewsDetailViewModel.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 03/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

class NewsDetailViewModel{
    
    public var article: Article
    
    //MARK: - constructor
    
    init(article: Article)
    {
        self.article = article
    }
    
}
