//
//  NewsDetailViewController.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 03/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailViewController: BaseViewController {

    @IBOutlet weak var titleNewsDetail: UILabel!
    @IBOutlet weak var dateNewsDetail: UILabel!
    @IBOutlet weak var imageNewsDetail: UIImageView!
    @IBOutlet weak var contentNewsDetail: UITextView!
    
    private var viewModel: NewsDetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControls()
    }
    
    public func setArticleDetail(article: Article){
        viewModel = NewsDetailViewModel(article: article)
    }
    
    //MARK: - Private methods
    private func loadImage() {
        let imageURL = viewModel?.article.urlToImage
        imageNewsDetail.kf.indicatorType = .activity
        imageNewsDetail.kf.setImage(with: imageURL)
    }
    
    
    private func initControls(){
        if let article = viewModel?.article{
            loadImage()
            titleNewsDetail.text = article.title
            dateNewsDetail.text = "\(K.TextValues.dateDetail) \(DateFormatter.getFormattedDate(date: article.publishedAt))"
            contentNewsDetail.text = article.content
        }
    }
}
