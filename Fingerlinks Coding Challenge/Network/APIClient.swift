//
//  APIClient.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    @discardableResult
    private static func performRequest<T:Decodable>(route:APIRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T>)->Void) -> DataRequest {
        return AF.request(route)
            .responseDecodable (decoder: decoder){ (response: DataResponse<T>) in
                print("Response \(response)")
                completion(response.result)
        }
    }
    
    
    static func getAppleArticles(completion:@escaping (Result<Root>)->Void) {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .formatted(.articleDateFormatter)
        //let gitData = try jsonDecoder.decode(Root.self, from: data!)
        performRequest(route: APIRouter.everything(), decoder: jsonDecoder, completion: completion)
    }

}
