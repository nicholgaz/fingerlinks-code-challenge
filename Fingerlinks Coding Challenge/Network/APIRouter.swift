//
//  ApiRouter.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: URLRequestConvertible {
    
    case topHeadlines()
    case everything()
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .topHeadlines:
            return .get
        case .everything :
            return .get
        }
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .topHeadlines:
            return "/top-headlines"
        case .everything:
            return "/everything"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .topHeadlines :
            return [K.APIParameters.apiKey : K.APIParameters.apiKeyValue]
        case .everything :
            return [K.APIParameters.questionSearch : K.APIParameters.questionSearchValue,K.APIParameters.sortBy: sortByValues.popularity.rawValue, K.APIParameters.apiKey : K.APIParameters.apiKeyValue]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        var encodedURLRequest: URLRequest
        // Parameters
        if let parameters = parameters {
            do {
                 encodedURLRequest = try URLEncoding.queryString.encode(urlRequest, with: parameters)
                print("XXX \(encodedURLRequest)")
                return encodedURLRequest
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        return urlRequest
    }
}
