//
//  NewsListViewController.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 03/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit

class NewsListViewController: BaseViewController, UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel: NewsListViewModel = NewsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControls()
    }
    
    //MARK: - Private methods
    private func initControls(){
        fetchLasAppleNews()
    }
    
    private func fetchLasAppleNews(){
        let id = UIViewController.postShowSpinnerNotification(message: "loading")
        
        self.viewModel.performNewsRequest() { (status: Bool, error: Error?) in
            
            UIViewController.postHideSpinnerNotification(spinnerId: id)
            
            if status
            {
                self.tableView.reloadData()
            }
            //TODO qui in un blocco else andrebbe messo un ViewController di Errore
        }
    }
    
    //MARK: - UI Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let newsListCell=tableView.dequeueReusableCell(withIdentifier: "newsListCell",for: indexPath) as! NewsListCell
        let article = viewModel.articles[indexPath.row] as Article
        newsListCell.titleNews.text = article.title
        newsListCell.dateNews.text = DateFormatter.getFormattedDate(date: article.publishedAt)
        return newsListCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }

    //MARK: - UITable View Data Source
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let selectedArticle = viewModel.articles[indexPath.row]
        let viewController = NewsDetailViewController.loadFromStoryboard()
        viewController.setArticleDetail(article: selectedArticle)
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
