//
//  NewsListViewModel.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 03/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

class NewsListViewModel{
public var viewTitle : String!


public var articles: [Article] = [Article]()

    //MARK: - constructor

    init()
    {
        self.initData()
    }

//MARK: - Private Methods

    private func initData()
    {
        self.viewTitle = K.TextValues.listNewsViewTitle

    }
    
    //MARK: - API Public Methods
    
    public func performNewsRequest(completionHandler:@escaping((_:Bool, _:Error?) -> Void))
    {
        APIClient.getAppleArticles{ result in
            switch result {
            case .success(let items):
                DispatchQueue.main.async{
                    if let articles = items.articles{
                        self.articles = articles
                    }
                    else{
                        self.articles = [Article]()
                    }
                    completionHandler(true, nil)
            }
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(false, error)
                  }
            }
        }
    }

}
