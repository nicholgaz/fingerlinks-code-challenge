//
//  NewsListCell.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 03/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import UIKit

class NewsListCell: UITableViewCell{
    
    @IBOutlet weak var titleNews: UILabel!
    @IBOutlet weak var dateNews: UILabel!
}
