//
//  Constants.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation
struct K {
    //Raggruppo le costanti in base alle categorie di utilizzo
    struct ProductionServer {
        static let baseURL = "https://newsapi.org/v2"
    }
    
    struct APIParameters {
        static let questionSearchValue = "apple"
        static let questionSearch = "q"
        static let sortBy = "sortBy"
        static let apiKey = "apiKey"
        static let apiKeyValue = "bde9b6e1f859479db564df3cb382f203"
    }
    
    struct TextValues {
        static let listNewsViewTitle = "Watch out last news about Apple"
        static let dateDetail = "published on"
    }
}

enum sortByValues: String {
    case relevancy = "relevancy"
    case popularity = "popularity"
    case publishedAt = "publishedAt"
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}
