//
//  DateFormatter.swift
//  Fingerlinks Coding Challenge
//
//  Created by Nicholas Caruso on 02/04/2019.
//  Copyright © 2019 Nicholas Caruso. All rights reserved.
//

import Foundation

extension DateFormatter {
    static var articleDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }
    
    static func getFormattedDate(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter.string(from: date)
    }
}
